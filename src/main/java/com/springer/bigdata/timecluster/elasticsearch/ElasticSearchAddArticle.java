package com.springer.bigdata.timecluster.elasticsearch;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.springer.bigdata.timecluster.model.Article;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class ElasticSearchAddArticle {


    private static final String ELASTIC_HOSTNAME = "localhost";
    public static final int ESPORT = 9300;
    public static final String ESINDEX = "semantic";
    public static final String ESTYPE = "article";

    public static Client client;

    public static void sendArticle(Article article) {
        client = new TransportClient().addTransportAddress(new InetSocketTransportAddress(ELASTIC_HOSTNAME, ESPORT));
        addArticle(article);
        client.close();
    }

    private static void addArticle(Article article) {

        // Create the JSON object
        try {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm'Z'").create();
            String articleJson = gson.toJson(article);

            System.out.println(articleJson);
            IndexRequest indexRequest = new IndexRequest(ESINDEX, ESTYPE, Long.toString(new GregorianCalendar().getTimeInMillis()))
                    .source(articleJson);
            IndexResponse response = client.index(indexRequest)
                    .actionGet();
            System.out.println("Response:" + response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
