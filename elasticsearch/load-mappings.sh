#!/bin/sh
set -o errexit

curl -u osadmin -XPUT 'http://localhost:9200/semantic/_mapping/article'  -d '
{
  "article": {
    "properties": {
      "journalId": {
        "type": "integer"
      },
      "doi": {
        "type": "string"
      },
      "title": {
        "type": "string",
        "analyzer": "english_analyzer",
        "_boost":  2.8
      },
      "abstractText": {
        "type": "string",
        "analyzer": "english_analyzer"
      },
      "fullText": {
        "type": "string",
        "analyzer": "english_analyzer"
      },
      "pubDate": {
        "type": "date"
      }
    }
  }
}
'