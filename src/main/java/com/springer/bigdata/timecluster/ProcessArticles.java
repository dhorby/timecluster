package com.springer.bigdata.timecluster;

import com.springer.bigdata.timecluster.elasticsearch.ElasticSearchAddArticle;
import com.springer.bigdata.timecluster.model.Article;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.Node;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProcessArticles {

    public static final String DOI_EXP = "/Publisher/Journal/Volume/Issue/Article/ArticleInfo/ArticleDOI";
    public static final String TITLE_EXP = "/Publisher/Journal/Volume/Issue/Article/ArticleInfo/ArticleTitle";
    public static final String ABSTRACT_TEXT_EXP = "/Publisher/Journal/Volume/Issue/Article/ArticleHeader/Abstract";
    public static final String JOURNAL_EXP = "/Publisher/Journal/JournalInfo/JournalID";
    public static final String PUB_DATE_EXP = "/Publisher/Info/Date";
    public static final String FULL_TEXT_EXP = "/Publisher/Journal/Volume/Issue/Article/Body";
    public static final SimpleDateFormat PARSE_DATE = new SimpleDateFormat("yyyy-mm-dd");

    public static void main(String[] args) {

        System.out.println("Args:" + args.length);

        // Uplaod articles to elasticsearch
        if (args.length != 1) {
            System.err.println("Arguments: [input directory] ");
            return;
        }
        String inputDirectory = args[0];

        List<URL> urls = new ArrayList();

        try {
            Files.walk(Paths.get(inputDirectory)).forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    System.out.println(filePath.toAbsolutePath());
                    try {
                        urls.add(filePath.toAbsolutePath().toUri().toURL());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        ProcessArticles processArticles = new ProcessArticles();
        List<Article> articles = processArticles.readAPlusPlus(urls);
        articles.forEach(article -> ElasticSearchAddArticle.sendArticle(article));
    }


    public static Article mapArticle  (URL xmlDocumentPath){

        Article article = new Article();

        XPath xPath =  XPathFactory.newInstance().newXPath();

        Document xmlDocument = getDocument(xmlDocumentPath);
        try {
            article.setDoi(xPath.compile(DOI_EXP).evaluate(xmlDocument));
            article.setTitle(xPath.compile(TITLE_EXP).evaluate(xmlDocument));
            article.setAbstractText(xPath.compile(ABSTRACT_TEXT_EXP).evaluate(xmlDocument));
            article.setJournalId(Integer.parseInt(xPath.compile(JOURNAL_EXP).evaluate(xmlDocument)));
            try {
                article.setPubDate(PARSE_DATE.parse(xPath.compile(PUB_DATE_EXP).evaluate(xmlDocument)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            article.setFullText(xPath.compile(FULL_TEXT_EXP).evaluate(xmlDocument).replaceAll("\\s+", " "));
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        System.out.println(article.toString());
        return article;


    }

    private static Document getDocument(URL xmlDocumentPath) {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        Document xmlDocument = null;
        try {
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            File inputFile = new File(xmlDocumentPath.getFile());
            System.out.println("Porcssing : " + xmlDocumentPath.getFile());
            xmlDocument = builder.parse(new FileInputStream(inputFile));
        } catch (SAXException e) {
            System.out.printf("Checking article:" + xmlDocumentPath.getFile());
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return xmlDocument;
    }

    ;



    public List<Article> readAPlusPlus(List<URL> urls)  {
        return urls.stream().map(doc -> mapArticle(doc)).filter(doc -> doc.getAbstractText().trim().length() > 10).collect(Collectors.toList());
    }
}
