#!/usr/bin/env bash
curl -XGET http://localhost:9200/semantic/article/1431710693549/_termvector?fields=title&pretty=true

curl -XPOST 'http://localhost:9200/_mtermvectors' -d '{
   "docs": [
      {
         "_index": "semantic",
         "_type": "article",
         "_id": "1431710693549",
         "term_statistics": true
      }
   ]
}'
