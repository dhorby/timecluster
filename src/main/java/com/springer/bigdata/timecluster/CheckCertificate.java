package com.springer.bigdata.timecluster;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.BasicHttpContext;
//import sun.misc.BASE64Encoder;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.Enumeration;

/**
 * Created by davidhorby on 07/07/15.
 */
public class CheckCertificate {

    public static void main(String[] args) {

        System.out.println("Started");
        CheckCertificate checkCertificate = new CheckCertificate();
        checkCertificate.checkCert();
//        checkCertificate.connectSSL();

        System.out.println("Finished");


    }

    private void connectSSL() {
//        String germanURL = "https://www.aai.dfn.de/fileadmin/metadata/DFN-AAI-metadata.xml";
        String germanURL = "http://www.google.co.uk";
        HttpGet request = new HttpGet(germanURL);
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpResponse response = httpClient.execute(request, new BasicHttpContext());
            System.out.println("Response Code : "
                    + response.getStatusLine().getStatusCode());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void checkCert() {
        try {
//            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
//            String keystoreLocation = "/Users/davidhorby/archive/fsso/dev-d/keystore.jks";
//            keystore.load(new FileInputStream(keystoreLocation), "changeit".toCharArray());
//            Enumeration<String> aliases = keystore.aliases();
//            while(aliases.hasMoreElements()){
//                String alias = aliases.nextElement();
//                if(keystore.getCertificate(alias).getType().equals("X.509")){
//                    Certificate cert = keystore.getCertificate(alias);
//                    System.out.println(alias + " expires " + ((X509Certificate) cert).getNotAfter());
//                    try {
//                        ((X509Certificate) cert).checkValidity();
//                    } catch (Exception e) {
//                        System.out.println("Invlaid::: " + alias);
//                        BASE64Encoder encoder = new BASE64Encoder();
//                        System.out.println("[[" + encoder.encodeBuffer(cert.getEncoded()) + "]]");
//                    }
//                }
//            }
//            String certString ="MIIDjTCCAnWgAwIBAgIBATANBgkqhkiG9w0BAQUFADCBiTELMAkGA1UEBhMCSVQxCzAJBgNVBAgTAk1JMQ4wDAYDVQQHEwVNaWxhbjEPMA0GA1UEChMGUE9MSU1JMQ4wDAYDVQQLEwVBU0lDVDEaMBgGA1UEAxMRc2hpYmlkcC5wb2xpbWkuaXQxIDAeBgkqhkiG9w0BCQEWEWhvc3RpbmdAcG9saW1pLml0MB4XDTE0MDIyMDExMDcwMFoXDTI2MDIyMDExMDcwMFowgYkxCzAJBgNVBAYTAklUMQswCQYDVQQIEwJNSTEOMAwGA1UEBxMFTWlsYW4xDzANBgNVBAoTBlBPTElNSTEOMAwGA1UECxMFQVNJQ1QxGjAYBgNVBAMTEXNoaWJpZHAucG9saW1pLml0MSAwHgYJKoZIhvcNAQkBFhFob3N0aW5nQHBvbGltaS5pdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKhtqK271QYDCWbYMZSqPdm29Oh3yeOVC+6FyoU9i3DIHaiqRxa1yKlYHLx5oLgtPi8ASDzXPfMo6MGVGYLOzCe/Y2efdJyz1Pqguna+8jfDtm1MeNCrjYd/cTp5gr/HGZGz262dSmIMWrWloF7Zg3jwsXvJYO0wCHLsKVRul7vdosPX7Jy9pIxTO1ZJMZCa/NASWMqf1i+BtYVpJqMQZJ85H9pEnu+6dBJMz6QM67zL9WnncMUz1ANkCkLX6mAFV9sydjTXPKrKhBBCZF44HOdiIbeZ+NKBZLeirVNALptmTxMg1bNYR2eeys9pdht7qdrvDcBziy5EtVFwmsFxMF8CAwEAATANBgkqhkiG9w0BAQUFAAOCAQEApV/JH6zxNIBuB/8zTbO7K/uJsh14u/ihkWxCwGJoZTKSttIwlH2Y0+R8fQW/+ZV/aOT3vzJzxJJ/WL6x9BkumlaYSnZ+zD0fQWWWSGK/ncqELDCE35h2B5/hmJ0wSHZewPTPRhvptIYsEFzr9Gr4hja1IylB5u+kXNOgR5XJvoojnI1855S/N1p6Y+JUfJKCTciw/0w8Kqlz5Hs9/GHtWRQMqSSzXV/Ti6l+6Mu2wvg0LSH2F9XZjN49ZX1E4LPcB8ChCzmrMf87+9+tru9CY00stxLKVpXdX8pXFj3zFusdHk7fSeE4cCUYQvN7+Jyy7uSEM10eED6xOqvPw31noA=";
            String dodgyCert = "MIIFQTCCBCmgAwIBAgIEUNShmjANBgkqhkiG9w0BAQsFADCBujELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUVudHJ1c3QsIEluYy4xKDAmBgNVBAsTH1NlZSB3d3cuZW50cnVzdC5uZXQvbGVnYWwtdGVybXMxOTA3BgNVBAsTMChjKSAyMDEyIEVudHJ1c3QsIEluYy4gLSBmb3IgYXV0aG9yaXplZCB1c2Ugb25seTEuMCwGA1UEAxMlRW50cnVzdCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eSAtIEwxSzAeFw0xNTA3MDYwNTQ3MTVaFw0xODA5MzAxNjQ0NDRaMH0xCzAJBgNVBAYTAkpQMQ4wDAYDVQQIEwVUb2t5bzEWMBQGA1UEBxMNSGFjaGlvaGppLXNoaTEmMCQGA1UEChMdVG9reW8gTWV0cm9wb2xpdGFuIFVuaXZlcnNpdHkxHjAcBgNVBAMTFWdha3VuaW5zaGliLnRtdS5hYy5qcDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALlxVotZvNCoi3phfNFaV9eHCi515DYwL85n9PoqxviQdBU8zuTaxucf/OGV2Lc65YlQehNdSCObUBJ4V0fsFhvuEdhqFyHC+WJ8Ble7UmsQnWU7hYne+IonTVRMgHPmA92tbG91VzgL853geIfxLMRvKi8CU+1m/Se2vlolCr4mVnqBwBXsSilhFtxD2WKM9P5W3bl6BfH4mAvjcYHS245OMW65FpqR18YzucQFIfhwhA31ngDbULadWCSw1cfpgPdjOdUhDQKfcT9igjOdd3YoGqfp770y5ndQ7EjYnxihGKeBiSt31XikJ0k9Y2prIzXraS1iBhCZrQUE9Nis+k0CAwEAAaOCAYkwggGFMAsGA1UdDwQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwMwYDVR0fBCwwKjAooCagJIYiaHR0cDovL2NybC5lbnRydXN0Lm5ldC9sZXZlbDFrLmNybDBLBgNVHSAERDBCMDYGCmCGSAGG+mwKAQUwKDAmBggrBgEFBQcCARYaaHR0cDovL3d3dy5lbnRydXN0Lm5ldC9ycGEwCAYGZ4EMAQICMGgGCCsGAQUFBwEBBFwwWjAjBggrBgEFBQcwAYYXaHR0cDovL29jc3AuZW50cnVzdC5uZXQwMwYIKwYBBQUHMAKGJ2h0dHA6Ly9haWEuZW50cnVzdC5uZXQvbDFrLWNoYWluMjU2LmNlcjAgBgNVHREEGTAXghVnYWt1bmluc2hpYi50bXUuYWMuanAwHwYDVR0jBBgwFoAUgqJwdN28Uz/Pe9T3zX+nYMYKTL8wHQYDVR0OBBYEFOhQe5YlCPSTCnb3FdAUftsJm7K9MAkGA1UdEwQCMAAwDQYJKoZIhvcNAQELBQADggEBAH5+7cpvL5h9h7Xkz30x+DuSS5oEajvVFvQXe2qG+qQuPmlJlCL5kCvi68UQ8xK5cULwEGaAGxLtsOAOL0etulbuss5FHA6BoaS7qhN9PO/TrTg6o3w8TmVK424RxRDlXs89G+3ECnb3RWeC6yNZFo3n0kLIzROwDH1YmdUKTpRuBZQlgSkvr/Swc3bP9w+1hkVAGaYezeKjjdCPq3Qc1/wV95oyiAj+hFudrqopB7noYbkaNTZF9EdmaKIU6lBglkqBxmyZbbnfUWXVYHk6vqdJdWQnm1iDkm9B3l/K+VedCSfwTG/oJKAWy1WS3TzZQuldYwxnVPiy059js0iNrJ0=";
            String certFile = "/Users/davidhorby/nl.cert";
            X509Certificate cert = CheckCertificate.readWWDRCertificate(dodgyCert);

            System.out.println("Certificate details:" + cert.getIssuerDN());
            System.out.println("Certificate validity:Between:" + cert.getNotBefore() + ":" + cert.getNotAfter());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static X509Certificate readWWDRCertificate(String keyFile) throws IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException
//    {
//        FileInputStream fis = null;
//        ByteArrayInputStream bais = null;
//        try
//        {
//            // use FileInputStream to read the file
//            fis = new FileInputStream(keyFile);
//
//            // read the bytes
//            byte value[] = new byte[fis.available()];
//            fis.read(value);
//            bais = new ByteArrayInputStream(value);
//
//            // get X509 certificate factory
//            CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
//
////            String certString ="MIIDvjCCAqagAwIBAgIEVOxCIjANBgkqhkiG9w0BAQsFADCBoDEoMCYGCSqGSIb3DQEJARYZYXRoZW5zaGVscEBlZHVzZXJ2Lm9yZy51azELMAkGA1UEBhMCR0IxETAPBgNVBAgMCFNvbWVyc2V0MQ0wCwYDVQQHDARCYXRoMRAwDgYDVQQKDAdFZHVzZXJ2MRMwEQYDVQQLDApPcGVuQXRoZW5zMR4wHAYDVQQDDBVnYXRld2F5LmF0aGVuc2Ftcy5uZXQwHhcNMTUwMjI0MDkyMDA2WhcNMjUwMjI0MDkyMDA2WjCBoDEoMCYGCSqGSIb3DQEJARYZYXRoZW5zaGVscEBlZHVzZXJ2Lm9yZy51azELMAkGA1UEBhMCR0IxETAPBgNVBAgMCFNvbWVyc2V0MQ0wCwYDVQQHDARCYXRoMRAwDgYDVQQKDAdFZHVzZXJ2MRMwEQYDVQQLDApPcGVuQXRoZW5zMR4wHAYDVQQDDBVnYXRld2F5LmF0aGVuc2Ftcy5uZXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCandpa4o0Njtw1DqbrrNTfOVe1PqyXIIVmDrJ6VUR/mokXXu+m5Gm+1f+3lyN5IA2YMn9Z8Yo37JQjIHs+xVS3q4nT1ewS7S3en1pdXKsH1WnUnVWUmpl9WJZrUwi5i8X80LNyr7PmudhuKNEATGUXkA/xWCkk2d8jf91hy7Qu+HA8LOKtdbbNigErh2IY/YuNWUVUqgGbMH5BGr7ZEhPrz+Vwcf9lhPW+tKpKpZEzJfQiq8EoPaeMXEpKWBEErm67gkWFCA5VhfcJLqFjQEC3pWOxt5rZVS8gl/Z33VSJZVzY5jWcQzmGaLXPHXyiKPmixl6+DjGlUM0ylNF7GvtDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFhmhujLZueiJ6F7mQCpfB0Hj4Y8FyFUUc8NMAt5Set7H4DKSSl4shcqisZBa5yTdyenYwkmBszvCWs6Yeep+zJmCR62cb/f1M32oMzLm02OlznWMkE8/IajGmdxTnB6Z/XcdMMIiCeok4kqe5KMd5oRAyNskHYZ+8kzhs2zTveR+rqCtYxa/AYpwf7n0VQR9clBSNCIT4BCRi10aPE531VIxl4ljY3CwNoZ4lQTU/0aj8O4j68V2neiQb8lewAii0b2xoyOGYP4okd7T2tl4gl2noVbCvYNjd6GYze/w4lgwiemkby7wu5sN1lEudgKDV+H54wU29ZIyDEFM6DDNE4=";
//            String certString = "MIID3zCCAsegAwIBAgIJAMVC9xn1ZfsuMA0GCSqGSIb3DQEBCwUAMIGFMQswCQYDVQQGEwJOTDEQMA4GA1UECAwHVXRyZWNodDEQMA4GA1UEBwwHVXRyZWNodDEVMBMGA1UECgwMU1VSRm5ldCBCLlYuMRMwEQYDVQQLDApTVVJGY29uZXh0MSYwJAYDVQQDDB1lbmdpbmUuc3VyZmNvbmV4dC5ubCAyMDE0MDUwNTAeFw0xNDA1MDUxNDIyMzVaFw0xOTA1MDUxNDIyMzVaMIGFMQswCQYDVQQGEwJOTDEQMA4GA1UECAwHVXRyZWNodDEQMA4GA1UEBwwHVXRyZWNodDEVMBMGA1UECgwMU1VSRm5ldCBCLlYuMRMwEQYDVQQLDApTVVJGY29uZXh0MSYwJAYDVQQDDB1lbmdpbmUuc3VyZmNvbmV4dC5ubCAyMDE0MDUwNTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKthMDbB0jKHefPzmRu9t2h7iLP4wAXr42bHpjzTEk6gttHFb4l/hFiz1YBI88TjiH6hVjnozo/YHA2c51us+Y7g0XoS7653lbUN/EHzvDMuyis4Xi2Ijf1A/OUQfH1iFUWttIgtWK9+fatXoGUS6tirQvrzVh6ZstEp1xbpo1SF6UoVl+fh7tM81qz+Crr/Kroan0UjpZOFTwxPoK6fdLgMAieKSCRmBGpbJHbQ2xxbdykBBrBbdfzIX4CDepfjE9h/40ldw5jRn3e392jrS6htk23N9BWWrpBT5QCk0kH3h/6F1Dm6TkyG9CDtt73/anuRkvXbeygI4wml9bL3rE8CAwEAAaNQME4wHQYDVR0OBBYEFD+Ac7akFxaMhBQAjVfvgGfY8hNKMB8GA1UdIwQYMBaAFD+Ac7akFxaMhBQAjVfvgGfY8hNKMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAC8L9D67CxIhGo5aGVu63WqRHBNOdo/FAGI7LURDFeRmG5nRw/VXzJLGJksh4FSkx7aPrxNWF1uFiDZ80EuYQuIv7bDLblK31ZEbdg1R9LgiZCdYSr464I7yXQY9o6FiNtSKZkQO8EsscJPPy/Zp4uHAnADWACkOUHiCbcKiUUFu66dX0Wr/v53Gekz487GgVRs8HEeT9MU1reBKRgdENR8PNg4rbQfLc3YQKLWK7yWnn/RenjDpuCiePj8N8/80tGgrNgK/6fzM3zI18sSywnXLswxqDb/J+jgVxnQ6MrsTf1urM8MnfcxG/82oHIwfMh/sXPCZpo+DTLkhQxctJ3M=";
//            String newStr="-----BEGIN CERTIFICATE-----\r\n" + certString + "\r\n-----END CERTIFICATE-----";
////            int n = bais.available();
////            byte[] bytes = new byte[n];
////            bais.read(bytes, 0, n);
////            String s = new String(bytes, StandardCharsets.UTF_8);
////            System.out.println(s);
//            System.out.println(newStr);
//            X509Certificate xNew= (X509Certificate)certFactory.generateCertificate(new ByteArrayInputStream(newStr.getBytes()));
//            // certificate factory can now create the certificate
//            return  (X509Certificate)certFactory.generateCertificate(new ByteArrayInputStream(newStr.getBytes()));
////            return (X509Certificate)certFactory.generateCertificate(bais);
//        }
//        finally
//        {
//            IOUtils.closeQuietly(fis);
//            IOUtils.closeQuietly(bais);
//        }
//    }



    public static X509Certificate readWWDRCertificate(String certString) throws IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException
    {
        FileInputStream fis = null;
        ByteArrayInputStream bais = null;
        try
        {
//            // use FileInputStream to read the file
//            fis = new FileInputStream(keyFile);
//
//            // read the bytes
//            byte value[] = new byte[fis.available()];
//            fis.read(value);
//            bais = new ByteArrayInputStream(value);

            // get X509 certificate factory
            CertificateFactory certFactory = CertificateFactory.getInstance("X.509");

//            String certString ="MIIDvjCCAqagAwIBAgIEVOxCIjANBgkqhkiG9w0BAQsFADCBoDEoMCYGCSqGSIb3DQEJARYZYXRoZW5zaGVscEBlZHVzZXJ2Lm9yZy51azELMAkGA1UEBhMCR0IxETAPBgNVBAgMCFNvbWVyc2V0MQ0wCwYDVQQHDARCYXRoMRAwDgYDVQQKDAdFZHVzZXJ2MRMwEQYDVQQLDApPcGVuQXRoZW5zMR4wHAYDVQQDDBVnYXRld2F5LmF0aGVuc2Ftcy5uZXQwHhcNMTUwMjI0MDkyMDA2WhcNMjUwMjI0MDkyMDA2WjCBoDEoMCYGCSqGSIb3DQEJARYZYXRoZW5zaGVscEBlZHVzZXJ2Lm9yZy51azELMAkGA1UEBhMCR0IxETAPBgNVBAgMCFNvbWVyc2V0MQ0wCwYDVQQHDARCYXRoMRAwDgYDVQQKDAdFZHVzZXJ2MRMwEQYDVQQLDApPcGVuQXRoZW5zMR4wHAYDVQQDDBVnYXRld2F5LmF0aGVuc2Ftcy5uZXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCandpa4o0Njtw1DqbrrNTfOVe1PqyXIIVmDrJ6VUR/mokXXu+m5Gm+1f+3lyN5IA2YMn9Z8Yo37JQjIHs+xVS3q4nT1ewS7S3en1pdXKsH1WnUnVWUmpl9WJZrUwi5i8X80LNyr7PmudhuKNEATGUXkA/xWCkk2d8jf91hy7Qu+HA8LOKtdbbNigErh2IY/YuNWUVUqgGbMH5BGr7ZEhPrz+Vwcf9lhPW+tKpKpZEzJfQiq8EoPaeMXEpKWBEErm67gkWFCA5VhfcJLqFjQEC3pWOxt5rZVS8gl/Z33VSJZVzY5jWcQzmGaLXPHXyiKPmixl6+DjGlUM0ylNF7GvtDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFhmhujLZueiJ6F7mQCpfB0Hj4Y8FyFUUc8NMAt5Set7H4DKSSl4shcqisZBa5yTdyenYwkmBszvCWs6Yeep+zJmCR62cb/f1M32oMzLm02OlznWMkE8/IajGmdxTnB6Z/XcdMMIiCeok4kqe5KMd5oRAyNskHYZ+8kzhs2zTveR+rqCtYxa/AYpwf7n0VQR9clBSNCIT4BCRi10aPE531VIxl4ljY3CwNoZ4lQTU/0aj8O4j68V2neiQb8lewAii0b2xoyOGYP4okd7T2tl4gl2noVbCvYNjd6GYze/w4lgwiemkby7wu5sN1lEudgKDV+H54wU29ZIyDEFM6DDNE4=";
//            String certString = "MIID3zCCAsegAwIBAgIJAMVC9xn1ZfsuMA0GCSqGSIb3DQEBCwUAMIGFMQswCQYDVQQGEwJOTDEQMA4GA1UECAwHVXRyZWNodDEQMA4GA1UEBwwHVXRyZWNodDEVMBMGA1UECgwMU1VSRm5ldCBCLlYuMRMwEQYDVQQLDApTVVJGY29uZXh0MSYwJAYDVQQDDB1lbmdpbmUuc3VyZmNvbmV4dC5ubCAyMDE0MDUwNTAeFw0xNDA1MDUxNDIyMzVaFw0xOTA1MDUxNDIyMzVaMIGFMQswCQYDVQQGEwJOTDEQMA4GA1UECAwHVXRyZWNodDEQMA4GA1UEBwwHVXRyZWNodDEVMBMGA1UECgwMU1VSRm5ldCBCLlYuMRMwEQYDVQQLDApTVVJGY29uZXh0MSYwJAYDVQQDDB1lbmdpbmUuc3VyZmNvbmV4dC5ubCAyMDE0MDUwNTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKthMDbB0jKHefPzmRu9t2h7iLP4wAXr42bHpjzTEk6gttHFb4l/hFiz1YBI88TjiH6hVjnozo/YHA2c51us+Y7g0XoS7653lbUN/EHzvDMuyis4Xi2Ijf1A/OUQfH1iFUWttIgtWK9+fatXoGUS6tirQvrzVh6ZstEp1xbpo1SF6UoVl+fh7tM81qz+Crr/Kroan0UjpZOFTwxPoK6fdLgMAieKSCRmBGpbJHbQ2xxbdykBBrBbdfzIX4CDepfjE9h/40ldw5jRn3e392jrS6htk23N9BWWrpBT5QCk0kH3h/6F1Dm6TkyG9CDtt73/anuRkvXbeygI4wml9bL3rE8CAwEAAaNQME4wHQYDVR0OBBYEFD+Ac7akFxaMhBQAjVfvgGfY8hNKMB8GA1UdIwQYMBaAFD+Ac7akFxaMhBQAjVfvgGfY8hNKMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAC8L9D67CxIhGo5aGVu63WqRHBNOdo/FAGI7LURDFeRmG5nRw/VXzJLGJksh4FSkx7aPrxNWF1uFiDZ80EuYQuIv7bDLblK31ZEbdg1R9LgiZCdYSr464I7yXQY9o6FiNtSKZkQO8EsscJPPy/Zp4uHAnADWACkOUHiCbcKiUUFu66dX0Wr/v53Gekz487GgVRs8HEeT9MU1reBKRgdENR8PNg4rbQfLc3YQKLWK7yWnn/RenjDpuCiePj8N8/80tGgrNgK/6fzM3zI18sSywnXLswxqDb/J+jgVxnQ6MrsTf1urM8MnfcxG/82oHIwfMh/sXPCZpo+DTLkhQxctJ3M=";
            String newStr="-----BEGIN CERTIFICATE-----\r\n" + certString + "\r\n-----END CERTIFICATE-----";
            System.out.println(newStr);
            X509Certificate xNew= (X509Certificate)certFactory.generateCertificate(new ByteArrayInputStream(newStr.getBytes()));
            // certificate factory can now create the certificate
            return  (X509Certificate)certFactory.generateCertificate(new ByteArrayInputStream(newStr.getBytes()));
//            return (X509Certificate)certFactory.generateCertificate(bais);
        }
        finally
        {
            IOUtils.closeQuietly(fis);
            IOUtils.closeQuietly(bais);
        }
    }
}
