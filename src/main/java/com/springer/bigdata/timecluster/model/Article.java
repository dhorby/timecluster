package com.springer.bigdata.timecluster.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Article {

    int journalId;

    String doi;

    String title;

    String abstractText = "";

    Date pubDate;

    String fullText;

    public Article() {
    }

    public String getDoi() {
        return doi;
    }
    public void setDoi(String category) {
        this.doi = doi;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getAbstractText() { return abstractText; }
    public void setAbstractText(String abstractText) {
        this.abstractText = abstractText;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String getFullText() {
        return fullText;
    }

    public int getJournalId() {
        return journalId;
    }

    public void setJournalId(int journalId) {
        this.journalId = journalId;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }



    public String toString() {
		return "DOI:" + doi + " Title:" + title + " Abstract:" + abstractText;
	}




}
