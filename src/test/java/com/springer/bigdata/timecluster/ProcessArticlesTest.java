package com.springer.bigdata.timecluster;

import com.springer.bigdata.timecluster.elasticsearch.ElasticSearchAddArticle;
import com.springer.bigdata.timecluster.model.Article;
import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by davidhorby on 15/05/15.
 */
public class ProcessArticlesTest  {

    @Ignore
    @Test
    public void checkLoadArticle() throws IOException {
        URL url = this.getClass().getResource("/s10393-004-0001-1.xml");
        System.out.println("Exists:" + url.getFile());
        ProcessArticles processArticles = new ProcessArticles();
        List<URL> urls = new ArrayList<>();
        List<Article> articles = new ArrayList<>();
        urls.add(url);
        articles = processArticles.readAPlusPlus(urls);

        assertThat(articles.size(), is(equalTo(1)));

    }

    @Ignore
    @Test
    public void checkLoadMultiplArticle() throws IOException {
        URL url1 = this.getClass().getResource("/s10393-004-0001-1.xml");
        URL url2 = this.getClass().getResource("/s10393-004-0002-0.xml");
        ProcessArticles processArticles = new ProcessArticles();
        List<URL> urls = new ArrayList<>();
        List<Article> articles = new ArrayList<>();
        urls.add(url1);
        urls.add(url2);
        articles = processArticles.readAPlusPlus(urls);
        assertThat(articles.size(), is(equalTo(2)));

    }

    @Ignore
    @Test
    public void sendToElasticSearch() {
        URL url = this.getClass().getResource("/s10393-004-0001-1.xml");
        ProcessArticles processArticles = new ProcessArticles();
        List<URL> urls = new ArrayList<>();
        List<Article> articles = new ArrayList<>();
        urls.add(url);

        articles = processArticles.readAPlusPlus(urls);

        assertThat(articles.size(), is(equalTo(1)));
        ElasticSearchAddArticle.sendArticle(articles.get(0));
    }

}