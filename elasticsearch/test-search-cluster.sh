#!/bin/sh
curl -XPOST http://localhost:9200/semantic/article/_search_with_clusters?pretty -d '
{
  "search_request":
  { "query": {"match_all" : {  }},
    "size": 1000
  },

  "max_hits": 0,
  "query_hint": "disease",
  "field_mapping": {
       "title"  : ["_source.title"],
       "abstractText": ["_source.abstractText"],
       "fullText": ["_source.fullText"]
  },
  "algorithm": "lingo"
}'